//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#pragma once

#include "StepTimer.h"
#include "DXFramework.h"
#include "Core/XUSG.h"
#include "RayTracing/XUSGRayTracing.h"

using namespace DirectX;

// Note that while ComPtr is used to manage the lifetime of resources on the CPU,
// it has no understanding of the lifetime of resources on the GPU. Apps must account
// for the GPU lifetime of resources to avoid destroying objects that may still be
// referenced by the GPU.
// An example of this can be found in the class method: OnDestroy().

class DXRTest : public DXFramework
{
public:
	DXRTest(uint32_t width, uint32_t height, std::wstring name);
	virtual ~DXRTest();

	virtual void OnInit();
	virtual void OnUpdate();
	virtual void OnRender();
	virtual void OnDestroy();

	virtual void OnKeyUp(uint8_t /*key*/);

private:
	static const uint32_t FrameCount = 3;
	static const uint32_t TextureWidth = 1024;
	static const uint32_t TextureHeight = 1024;
	static const uint32_t TexturePixelSize = 4;	// The number of bytes used to represent a pixel in the texture.

	struct Vertex
	{
		XMFLOAT3 position;
		XMFLOAT2 uv;
	};

	XUSG::ShaderPool			m_shaderPool;
	//XUSG::Graphics::PipelineCache	m_pipelineCache;
	XUSG::PipelineLayoutCache	m_pipelineLayoutCache;
	XUSG::DescriptorTableCache	m_descriptorTableCache;

	XUSG::PipelineLayout		m_pipelineLayout;
	//XUSG::InputLayout 		m_inputLayout;

	// Pipeline objects.
	XUSG::Viewport				m_viewport;
	XUSG::RectRange				m_scissorRect;

	XUSG::SwapChain				m_swapChain;
	XUSG::CommandAllocator		m_commandAllocators[FrameCount];
	XUSG::CommandQueue			m_commandQueue;

	bool m_isDxrSupported;

	XUSG::RayTracing::Device m_device;
	XUSG::RenderTarget m_renderTargets[FrameCount];
	//XUSG::Pipeline	m_pipeline;
	XUSG::RayTracing::CommandList m_commandList;

	// App resources.
	XUSG::DescriptorPool	m_rtvPool;
	//XUSG::RenderTargetTable	m_rtvTables[FrameCount];
	//XUSG::DescriptorTable	m_cbvTables[FrameCount];
	//XUSG::DescriptorTable	m_srvTable;
	XUSG::DescriptorTable	m_uavTable;
	XUSG::DescriptorTable	m_srvIATable;
	XUSG::DescriptorTable	m_srvTextureTable;
	XUSG::DescriptorTable	m_samplerTable;

	XUSG::DepthStencil	m_depth;
	XUSG::VertexBuffer	m_vertexBuffer;
	XUSG::IndexBuffer	m_indexBuffer;
	//XUSG::ConstantBuffer m_constantBuffer;
	XUSG::Texture2D		m_textures[8];
	XUSG::Texture2D		m_raytracingOutput;
	XMFLOAT4 m_cbData_Viewport;
	XMFLOAT4 m_cbData_Offset;

	// Synchronization objects.
	uint32_t	m_frameIndex;
	HANDLE		m_fenceEvent;
	XUSG::Fence	m_fence;
	uint64_t	m_fenceValues[FrameCount];

	// Application state
	bool		m_pausing;
	StepTimer	m_timer;

	void LoadPipeline();
	void LoadAssets();
	std::vector<uint8_t> GenerateTextureData(uint32_t subDivLevel);
	void PopulateCommandList();
	void WaitForGpu();
	void MoveToNextFrame();
	double CalculateFrameStats(float* fTimeStep = nullptr);

	// Ray tracing
	void EnableDirectXRaytracing(IDXGIAdapter1* adapter);
	void CreateRaytracingInterfaces();
	void CreateRaytracingPipelineLayouts();
	void CreateRaytracingPipeline();
	void CreateDescriptorTables(std::vector<XUSG::Descriptor>& descriptors);
	void BuildAccelerationStructures(uint32_t numDescs, XUSG::RayTracing::Geometry* geometries,
		XUSG::Resource& scratchResource, XUSG::Resource& instanceDescs);
	void BuildShaderTables();
	void RayTrace();
	void CopyRaytracingOutputToBackbuffer();

	XUSG::RayTracing::PipelineCache	m_pipelineCache;

	static const uint32_t NumUAVs = 3;
	XUSG::RayTracing::BottomLevelAS m_bottomLevelAS;
	XUSG::RayTracing::TopLevelAS m_topLevelAS;

	XUSG::PipelineLayout m_globalPipelineLayout;
	XUSG::PipelineLayout m_rayGenPipelineLayout;
	XUSG::PipelineLayout m_hitGroupPipelineLayout;
	XUSG::RayTracing::Pipeline m_pipeline;

	// Shader tables
	static const wchar_t* HitGroupName;
	static const wchar_t* RaygenShaderName;
	static const wchar_t* ClosestHitShaderName;
	static const wchar_t* MissShaderName;
	XUSG::RayTracing::ShaderTable m_missShaderTable;
	XUSG::RayTracing::ShaderTable m_hitGroupShaderTable;
	XUSG::RayTracing::ShaderTable m_rayGenShaderTable;
};
