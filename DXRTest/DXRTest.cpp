//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#include "DXRTest.h"

//#define SizeOfInUint32(obj) DIV_UP(sizeof(obj), sizeof(uint32_t))

using namespace std;
using namespace XUSG;
using namespace XUSG::RayTracing;

enum VertexShader
{
	VS_TRIANGLE,

	NUM_VS
};

enum PixelShader
{
	PS_TRIANGLE,

	NUM_PS
};

const wchar_t* DXRTest::HitGroupName = L"MyHitGroup";
const wchar_t* DXRTest::RaygenShaderName = L"MyRaygenShader";
const wchar_t* DXRTest::ClosestHitShaderName = L"MyClosestHitShader";
const wchar_t* DXRTest::MissShaderName = L"MyMissShader";

DXRTest::DXRTest(uint32_t width, uint32_t height, std::wstring name) :
	DXFramework(width, height, name),
	m_isDxrSupported(false),
	m_frameIndex(0),
	m_viewport(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height)),
	m_scissorRect(0, 0, static_cast<long>(width), static_cast<long>(height))
{
#if defined (_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	AllocConsole();
	FILE* stream;
	freopen_s(&stream, "CONOUT$", "w+t", stdout);
	freopen_s(&stream, "CONIN$", "r+t", stdin);
#endif

	m_cbData_Viewport = { -1.0f, 1.0f, 1.0f, -1.0f };
}

DXRTest::~DXRTest()
{
#if defined (_DEBUG)
	FreeConsole();
#endif
}

void DXRTest::OnInit()
{
	LoadPipeline();
	LoadAssets();
}

// Load the rendering pipeline dependencies.
void DXRTest::LoadPipeline()
{
	auto dxgiFactoryFlags = 0u;

#if defined(_DEBUG)
	// Enable the debug layer (requires the Graphics Tools "optional feature").
	// NOTE: Enabling the debug layer after device creation will invalidate the active device.
	{
		ComPtr<ID3D12Debug1> debugController;
		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
		{
			debugController->EnableDebugLayer();
			debugController->SetEnableGPUBasedValidation(TRUE);

			// Enable additional debug layers.
			dxgiFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
		}
	}
#endif

	com_ptr<IDXGIFactory4> factory;
	ThrowIfFailed(CreateDXGIFactory2(dxgiFactoryFlags, IID_PPV_ARGS(&factory)));

	DXGI_ADAPTER_DESC1 dxgiAdapterDesc;
	com_ptr<IDXGIAdapter1> dxgiAdapter;
	auto hr = DXGI_ERROR_UNSUPPORTED;
	for (auto i = 0u; hr == DXGI_ERROR_UNSUPPORTED; ++i)
	{
		dxgiAdapter = nullptr;
		ThrowIfFailed(factory->EnumAdapters1(i, &dxgiAdapter));
		EnableDirectXRaytracing(dxgiAdapter.get());
		hr = D3D12CreateDevice(dxgiAdapter.get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&m_device.Common));
	}

	dxgiAdapter->GetDesc1(&dxgiAdapterDesc);
	if (dxgiAdapterDesc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
		m_title += dxgiAdapterDesc.VendorId == 0x1414 && dxgiAdapterDesc.DeviceId == 0x8c ? L" (WARP)" : L" (Software)";
	ThrowIfFailed(hr);

	// Create the command queue.
	N_RETURN(m_device.Common->GetCommandQueue(m_commandQueue, CommandListType::DIRECT, CommandQueueFlags::NONE), ThrowIfFailed(E_FAIL));

	// Describe and create the swap chain.
	DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
	swapChainDesc.BufferCount = FrameCount;
	swapChainDesc.Width = m_width;
	swapChainDesc.Height = m_height;
	swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.SampleDesc.Count = 1;

	ComPtr<IDXGISwapChain1> swapChain;
	ThrowIfFailed(factory->CreateSwapChainForHwnd(
		m_commandQueue.get(),		// Swap chain needs the queue so that it can force a flush on it.
		Win32Application::GetHwnd(),
		&swapChainDesc,
		nullptr,
		nullptr,
		&swapChain
	));

	// This sample does not support fullscreen transitions.
	ThrowIfFailed(factory->MakeWindowAssociation(Win32Application::GetHwnd(), DXGI_MWA_NO_ALT_ENTER));

	ThrowIfFailed(swapChain.As(&m_swapChain));
	m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();

	m_descriptorTableCache.SetDevice(m_device.Common);

	// Create frame resources.
	{
		// Create a RTV and a command allocator for each frame.
		for (auto n = 0u; n < FrameCount; n++)
		{
			N_RETURN(m_renderTargets[n].CreateFromSwapChain(m_device.Common, m_swapChain, n), ThrowIfFailed(E_FAIL));
			N_RETURN(m_device.Common->GetCommandAllocator(m_commandAllocators[n], CommandListType::DIRECT), ThrowIfFailed(E_FAIL));
		}
	}

	// Create a DSV
	N_RETURN(m_depth.Create(m_device.Common, m_width, m_height, Format::D24_UNORM_S8_UINT,
		ResourceFlag::DENY_SHADER_RESOURCE), ThrowIfFailed(E_FAIL));
}

// Load the sample assets.
void DXRTest::LoadAssets()
{
	//m_pipelineCache.SetDevice(m_device.Common);
	m_pipelineLayoutCache.SetDevice(m_device.Common);

	// Original DX12 graphics related, for future reference 
#if 0
	// Create the root signature.
	{
		Util::PipelineLayout pipelineLayout;
		pipelineLayout.SetRange(0, DescriptorType::CBV, 1, 0);
		pipelineLayout.SetRange(1, DescriptorType::SRV, static_cast<uint32_t>(size(m_textures)), 0);
		pipelineLayout.SetRange(1, DescriptorType::UAV, 1, 0);
		pipelineLayout.SetRange(2, DescriptorType::SAMPLER, 1, 0);
		pipelineLayout.SetShaderStage(0, Shader::Stage::VS);
		pipelineLayout.SetShaderStage(1, Shader::Stage::PS);
		pipelineLayout.SetShaderStage(2, Shader::Stage::PS);
		m_pipelineLayout = pipelineLayout.GetPipelineLayout(m_pipelineLayoutCache, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);
	}

	// Create the pipeline state, which includes compiling and loading shaders.
	{
		m_shaderPool.CreateShader(Shader::Stage::VS, VS_TRIANGLE, L"VertexShader.cso");
		m_shaderPool.CreateShader(Shader::Stage::PS, PS_TRIANGLE, L"PixelShader.cso");

		// Define the vertex input layout.
		InputElementTable inputElementDescs =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
		};

		m_inputLayout = m_pipelineCache.CreateInputLayout(inputElementDescs);

		// Describe and create the graphics pipeline state object (PSO).
		Graphics::State state;
		state.IASetInputLayout(m_inputLayout);
		state.SetPipelineLayout(m_pipelineLayout);
		state.SetShader(Shader::Stage::VS, m_shaderPool.GetShader(Shader::Stage::VS, VS_TRIANGLE));
		state.SetShader(Shader::Stage::PS, m_shaderPool.GetShader(Shader::Stage::PS, PS_TRIANGLE));
		//state.DSSetState(Graphics::DepthStencilPreset::DEPTH_STENCIL_NONE, m_pipelinePool);
		state.IASetPrimitiveTopologyType(D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE);
		state.OMSetNumRenderTargets(1);
		state.OMSetRTVFormat(0, DXGI_FORMAT_R8G8B8A8_UNORM);
		state.OMSetDSVFormat(DXGI_FORMAT_D24_UNORM_S8_UINT);
		m_pipeline = state.GetPipeline(m_pipelineCache);
	}
#endif

	// Create the command list.
	N_RETURN(m_device.Common->GetCommandList(m_commandList.GetCommandList(), 0, CommandListType::DIRECT,
		m_commandAllocators[m_frameIndex], nullptr), ThrowIfFailed(E_FAIL));

	// Create ray tracing interfaces
	CreateRaytracingInterfaces();

	// Create the ray-tracing root signature.
	CreateRaytracingPipelineLayouts();

	// Create a raytracing pipeline state object which defines the binding of shaders, state and resources to be used during raytracing.
	CreateRaytracingPipeline();

	// Create the vertex buffer.
	Resource vertexUpload;
	{
		// Define the geometry for a triangle.
		Vertex triangleVertices[] =
		{
			{ { 0.0f, 0.25f * m_aspectRatio, 0.0f }, { 0.5f, 0.0f } },
			{ { 0.25f, -0.25f * m_aspectRatio, 0.0f }, { 1.0f, 1.0f } },
			{ { -0.25f, -0.25f * m_aspectRatio, 0.0f }, { 0.0f, 1.0f } }
		};

		m_vertexBuffer.Create(m_device.Common, static_cast<uint32_t>(size(triangleVertices)), sizeof(Vertex));
		m_vertexBuffer.Upload(m_commandList, vertexUpload, triangleVertices,
			sizeof(triangleVertices), 0, ResourceState::NON_PIXEL_SHADER_RESOURCE);
	}

	// Create the index buffer.
	Resource indexUpload;
	{
		// Define the geometry for a triangle.
		uint16_t triangleIndices[] = { 0, 1, 2 };
		const uint32_t indexBufferSize = sizeof(triangleIndices);

		m_indexBuffer.Create(m_device.Common, indexBufferSize, Format::R16_UINT, ResourceFlag::NONE);
		m_indexBuffer.Upload(m_commandList, indexUpload, triangleIndices,
			indexBufferSize, 0, ResourceState::NON_PIXEL_SHADER_RESOURCE);
	}

	// Note: ComPtr's are CPU objects but this resource needs to stay in scope until
	// the command list that references it has finished executing on the GPU.
	// We will flush the GPU at the end of this method to ensure the resource is not
	// prematurely destroyed.
	Resource textureUploads[extent<decltype(m_textures)>::value];

#if 0
	// Create the constant buffer.
	{
		m_constantBuffer.Create(m_device.Common, sizeof(XMFLOAT4) * FrameCount, FrameCount);

		for (auto i = 0ui8; i < FrameCount; ++i)
		{
			Util::DescriptorTable cbvTable;
			cbvTable.SetDescriptors(0, 1, &m_constantBuffer.GetCBV(i));
			m_cbvTables[i] = cbvTable.GetCbvSrvUavTable(m_descriptorTableCache);
		}
	}
#endif

	// Create the textures.
	{
		// Copy data to the intermediate upload heap and then schedule a copy 
		// from the upload heap to the Texture2D.
		vector<Descriptor> srvUavs(static_cast<uint32_t>(size(m_textures)) + 1);
		for (auto i = 0u; i < static_cast<uint32_t>(size(m_textures)); ++i)
		{
			const auto texture = GenerateTextureData(i);
			m_textures[i].Create(m_device.Common, TextureWidth, TextureHeight, Format::R8G8B8A8_UNORM);
			m_textures[i].Upload(m_commandList, textureUploads[i], texture.data(),
				TexturePixelSize, ResourceState::NON_PIXEL_SHADER_RESOURCE);
			srvUavs[i] = m_textures[i].GetSRV();
		}

		m_raytracingOutput.Create(m_device.Common, m_width, m_height, Format::R8G8B8A8_UNORM,
			1, ResourceFlag::ALLOW_UNORDERED_ACCESS);
		srvUavs[static_cast<uint32_t>(size(m_textures))] = m_raytracingOutput.GetUAV();
#if 0
		Util::DescriptorTable srvUavTable;
		srvUavTable.SetDescriptors(0, static_cast<uint32_t>(size(m_textures)) + 1, srvUavs.data());
		m_srvTable = srvUavTable.GetCbvSrvUavTable(m_descriptorTableCache);
#endif
	}

	// Create the sampler
	{
		Util::DescriptorTable samplerTable;
		const auto samplerAnisoWrap = SamplerPreset::ANISOTROPIC_WRAP;
		samplerTable.SetSamplers(0, 1, &samplerAnisoWrap, m_descriptorTableCache);
		m_samplerTable = samplerTable.GetSamplerTable(m_descriptorTableCache);
	}

	// Setup the acceleration structures (AS) for raytracing. When setting up geometry, each
	// bottom-level AS has its own transform matrix.
	Resource scratchResource, instanceDescs;
	Geometry geometries[1];
	BuildAccelerationStructures(1, geometries, scratchResource, instanceDescs);

	// Build shader tables, which define shaders and their local root arguments.
	BuildShaderTables();

	// Close the command list and execute it to begin the initial GPU setup.
	ThrowIfFailed(m_commandList.Close());
	BaseCommandList* const ppCommandLists[] = { m_commandList.GetCommandList().get() };
	m_commandQueue->ExecuteCommandLists(static_cast<uint32_t>(size(ppCommandLists)), ppCommandLists);

	// Create synchronization objects and wait until assets have been uploaded to the GPU.
	{
		N_RETURN(m_device.Common->GetFence(m_fence, m_fenceValues[m_frameIndex]++, FenceFlag::NONE), ThrowIfFailed(E_FAIL));

		// Create an event handle to use for frame synchronization.
		m_fenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		if (m_fenceEvent == nullptr)
		{
			ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
		}

		// Wait for the command list to execute; we are reusing the same command 
		// list in our main loop but for now, we just want to wait for setup to 
		// complete before continuing.
		WaitForGpu();
	}
}

// Generate a simple black and white checkerboard texture.
vector<uint8_t> DXRTest::GenerateTextureData(uint32_t subDivLevel)
{
	const auto rowPitch = TextureWidth * TexturePixelSize;
	const auto cellPitch = rowPitch >> subDivLevel;			// The width of a cell in the checkboard texture.
	const auto cellHeight = TextureWidth >> subDivLevel;	// The height of a cell in the checkerboard texture.
	const auto textureSize = rowPitch * TextureHeight;

	vector<uint8_t> data(textureSize);
	uint8_t* pData = &data[0];

	for (auto n = 0u; n < textureSize; n += TexturePixelSize)
	{
		auto x = n % rowPitch;
		auto y = n / rowPitch;
		auto i = x / cellPitch;
		auto j = y / cellHeight;

		if (i % 2 == j % 2)
		{
			pData[n] = 0x00;		// R
			pData[n + 1] = 0x00;	// G
			pData[n + 2] = 0x00;	// B
			pData[n + 3] = 0xff;	// A
		}
		else
		{
			pData[n] = 0xff;		// R
			pData[n + 1] = 0xff;	// G
			pData[n + 2] = 0xff;	// B
			pData[n + 3] = 0xff;	// A
		}
	}

	return data;
}

// Update frame-based values.
void DXRTest::OnUpdate()
{
	// Timer
	static auto time = 0.0, pauseTime = 0.0;

	m_timer.Tick();
	const auto totalTime = CalculateFrameStats();
	pauseTime = m_pausing ? totalTime - time : pauseTime;
	time = totalTime - pauseTime;

	// Animation
	const float translationSpeed = 0.001f;
	const float offsetBounds = 1.25f;

	m_cbData_Offset.x += translationSpeed;
	if (m_cbData_Offset.x > offsetBounds)
	{
		m_cbData_Offset.x = -offsetBounds;
	}

#if 0
	// Map and initialize the constant buffer. We don't unmap this until the
	// app closes. Keeping things mapped for the lifetime of the resource is okay.
	const auto pCbData = reinterpret_cast<XMFLOAT4*>(m_constantBuffer.Map(m_frameIndex));
	*pCbData = m_cbData_Offset;
#endif

	// Get shader identifiers.
	const auto shaderIDSize = ShaderRecord::GetShaderIDSize(m_device);

	// Ray gen shader table
	XMFLOAT4 rayGenArgs[] = { m_cbData_Viewport, m_cbData_Offset };
	m_rayGenShaderTable.Reset();
	m_rayGenShaderTable.AddShaderRecord(ShaderRecord(m_device, m_pipeline,
		RaygenShaderName, rayGenArgs, sizeof(rayGenArgs)));
}

// Render the scene.
void DXRTest::OnRender()
{
	// Record all the commands we need to render the scene into the command list.
	PopulateCommandList();

	// Execute the command list.
	BaseCommandList* const ppCommandLists[] = { m_commandList.GetCommandList().get() };
	m_commandQueue->ExecuteCommandLists(static_cast<uint32_t>(size(ppCommandLists)), ppCommandLists);

	// Present the frame.
	ThrowIfFailed(m_swapChain->Present(0, 0));

	MoveToNextFrame();
}

void DXRTest::OnDestroy()
{
	// Ensure that the GPU is no longer referencing resources that are about to be
	// cleaned up by the destructor.
	WaitForGpu();

	CloseHandle(m_fenceEvent);
}

// User hot-key interactions.
void DXRTest::OnKeyUp(uint8_t key)
{
	switch (key)
	{
	case 0x20:	// case VK_SPACE:
		m_pausing = !m_pausing;
		break;
	}
}

void DXRTest::PopulateCommandList()
{
	// Command list allocators can only be reset when the associated 
	// command lists have finished execution on the GPU; apps should use 
	// fences to determine GPU execution progress.
	ThrowIfFailed(m_commandAllocators[m_frameIndex]->Reset());

	// However, when ExecuteCommandList() is called on a particular command 
	// list, that command list can then be reset at any time and must be before 
	// re-recording.
	ThrowIfFailed(m_commandList.Reset(m_commandAllocators[m_frameIndex], nullptr));

	// Indicate that the back buffer will be used as a render target.
	//const float clearColor[] = { 0.0f, 0.2f, 0.4f, 1.0f };
	//m_commandList.Barrier(1, &ResourceBarrier::Transition(m_renderTargets[m_frameIndex].get(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));
	//m_commandList.ClearRenderTargetView(*m_rtvTables[m_frameIndex], clearColor, 0, nullptr);

	// Record commands.
	ResourceBarrier barrier;
	const auto numBarriers = m_raytracingOutput.SetBarrier(&barrier, ResourceState::UNORDERED_ACCESS);
	m_commandList.Barrier(numBarriers, &barrier);
	RayTrace();

#if 0
	// Set necessary state.
	m_commandList.SetPipelineState(m_pipeline);
	m_commandList.SetGraphicsPipelineLayout(m_pipelineLayout);

	const DescriptorPool descriptorPools[] =
	{
		m_descriptorTableCache.GetDescriptorPool(CBV_SRV_UAV_POOL),
		m_descriptorTableCache.GetDescriptorPool(SAMPLER_POOL)
	};
	m_commandList.SetDescriptorPools(static_cast<uint32_t>(size(descriptorPools)), descriptorPools);

	m_commandList.SetGraphicsDescriptorTable(0, m_cbvTables[m_frameIndex]);
	m_commandList.SetGraphicsDescriptorTable(1, m_srvTable);
	m_commandList.SetGraphicsDescriptorTable(2, m_samplerTable);
	m_commandList.RSSetViewports(1, &m_viewport);
	m_commandList.RSSetScissorRects(1, &m_scissorRect);

	m_commandList.OMSetRenderTargets(0, nullptr, m_depth.GetDSV());
	//m_commandList.OMSetRenderTargets(1, &m_renderTargets[m_frameIndex].GetRTV(), m_depth.GetDSV());

	// Record commands.
	m_commandList.ClearDepthStencilView(m_depth.GetDSV(), ClearFlag::DEPTH, 1.0f, 0, 0, nullptr);
	m_commandList.IASetPrimitiveTopology(PrimitiveTopology::TRIANGLELIST);
	m_commandList.IASetVertexBuffers(0, 1, &m_vertexBuffer.GetVBV());
	m_commandList.IASetIndexBuffer(m_indexBuffer.GetIBV());
	m_commandList.DrawIndexed(3, 2, 0, 0, 0);
#endif

	CopyRaytracingOutputToBackbuffer();

	ThrowIfFailed(m_commandList.Close());
}

// Wait for pending GPU work to complete.
void DXRTest::WaitForGpu()
{
	// Schedule a Signal command in the queue.
	ThrowIfFailed(m_commandQueue->Signal(m_fence.get(), m_fenceValues[m_frameIndex]));

	// Wait until the fence has been processed, and increment the fence value for the current frame.
	ThrowIfFailed(m_fence->SetEventOnCompletion(m_fenceValues[m_frameIndex]++, m_fenceEvent));
	WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);
}

// Prepare to render the next frame.
void DXRTest::MoveToNextFrame()
{
	// Schedule a Signal command in the queue.
	const auto currentFenceValue = m_fenceValues[m_frameIndex];
	ThrowIfFailed(m_commandQueue->Signal(m_fence.get(), currentFenceValue));

	// Update the frame index.
	m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();

	// If the next frame is not ready to be rendered yet, wait until it is ready.
	if (m_fence->GetCompletedValue() < m_fenceValues[m_frameIndex])
	{
		ThrowIfFailed(m_fence->SetEventOnCompletion(m_fenceValues[m_frameIndex], m_fenceEvent));
		WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);
	}

	// Set the fence value for the next frame.
	m_fenceValues[m_frameIndex] = currentFenceValue + 1;
}

double DXRTest::CalculateFrameStats(float* fTimeStep)
{
	static int frameCnt = 0;
	static double elapsedTime = 0.0;
	const auto totalTime = m_timer.GetTotalSeconds();
	++frameCnt;

	const auto timeStep = static_cast<float>(totalTime - elapsedTime);

	// Compute averages over one second period.
	if ((totalTime - elapsedTime) >= 1.0f)
	{
		float fps = static_cast<float>(frameCnt) / timeStep;	// Normalize to an exact second.

		frameCnt = 0;
		elapsedTime = totalTime;

		wstringstream windowText;
		windowText << setprecision(2) << fixed << L"    fps: " << fps;
		SetCustomWindowText(windowText.str().c_str());
	}

	if (fTimeStep)* fTimeStep = timeStep;

	return totalTime;
}

//--------------------------------------------------------------------------------------
// Ray tracing
//--------------------------------------------------------------------------------------

// Enable experimental features required for compute-based raytracing fallback.
// This will set active D3D12 devices to DEVICE_REMOVED state.
// Returns bool whether the call succeeded and the device supports the feature.
inline bool EnableComputeRaytracingFallback(IDXGIAdapter1* adapter)
{
	ComPtr<ID3D12Device> testDevice;
	UUID experimentalFeatures[] = { D3D12ExperimentalShaderModels };

	return SUCCEEDED(D3D12EnableExperimentalFeatures(1, experimentalFeatures, nullptr, nullptr))
		&& SUCCEEDED(D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&testDevice)));
}

// Returns bool whether the device supports DirectX Raytracing tier.
inline bool IsDirectXRaytracingSupported(IDXGIAdapter1* adapter)
{
	ComPtr<ID3D12Device> testDevice;
	D3D12_FEATURE_DATA_D3D12_OPTIONS5 featureSupportData = {};

	return SUCCEEDED(D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&testDevice)))
		&& SUCCEEDED(testDevice->CheckFeatureSupport(D3D12_FEATURE_D3D12_OPTIONS5, &featureSupportData, sizeof(featureSupportData)))
		&& featureSupportData.RaytracingTier != D3D12_RAYTRACING_TIER_NOT_SUPPORTED;
}

void DXRTest::EnableDirectXRaytracing(IDXGIAdapter1* adapter)
{
	// Fallback Layer uses an experimental feature and needs to be enabled before creating a D3D12 device.
	bool isFallbackSupported = EnableComputeRaytracingFallback(adapter);

	if (!isFallbackSupported)
	{
		OutputDebugString(
			L"Warning: Could not enable Compute Raytracing Fallback (D3D12EnableExperimentalFeatures() failed).\n" \
			L"         Possible reasons: your OS is not in developer mode.\n\n");
	}

	m_isDxrSupported = IsDirectXRaytracingSupported(adapter);

	if (!m_isDxrSupported)
	{
		OutputDebugString(L"Warning: DirectX Raytracing is not supported by your GPU and driver.\n\n");

		if (!isFallbackSupported)
			OutputDebugString(L"Could not enable compute based fallback raytracing support (D3D12EnableExperimentalFeatures() failed).\n"\
				L"Possible reasons: your OS is not in developer mode.\n\n");
		ThrowIfFailed(isFallbackSupported ? S_OK : E_FAIL);
		m_device.RaytracingAPI = RayTracing::API::FallbackLayer;
	}
}

void DXRTest::CreateRaytracingInterfaces()
{
	if (m_device.RaytracingAPI == RayTracing::API::FallbackLayer)
	{
		const auto createDeviceFlags = CreateRaytracingFallbackDeviceFlags::EnableRootDescriptorsInShaderRecords;
		ThrowIfFailed(D3D12CreateRaytracingFallbackDevice(m_device.Common.get(), createDeviceFlags, 0, IID_PPV_ARGS(&m_device.Fallback)));
	}
	else // DirectX Raytracing
	{
		const auto hr = m_device.Common->QueryInterface(IID_PPV_ARGS(&m_device.Native));
		if (FAILED(hr)) OutputDebugString(L"Couldn't get DirectX Raytracing interface for the device.\n");
		ThrowIfFailed(hr);
	}

	m_commandList.CreateRaytracingInterfaces(m_device);
}

void DXRTest::CreateRaytracingPipelineLayouts()
{
	// Global Root Signature
	// This is a root signature that is shared across all raytracing shaders invoked during a DispatchRays() call.
	{
		RayTracing::PipelineLayout pipelineLayout;
		pipelineLayout.SetRange(0, DescriptorType::UAV, 1, 0);
		pipelineLayout.SetRootSRV(1, 0);
		pipelineLayout.SetRange(2, DescriptorType::SAMPLER, 1, 0);
		pipelineLayout.SetRange(3, DescriptorType::SRV, 2, 1);
		pipelineLayout.SetRange(4, DescriptorType::SRV, static_cast<uint32_t>(size(m_textures)), 0, 1);
		m_globalPipelineLayout = pipelineLayout.GetPipelineLayout(m_device, m_pipelineLayoutCache,
			PipelineLayoutFlag::NONE, NumUAVs);
	}

	// Local Root Signature for RayGen shader
	// This is a root signature that enables a shader to have unique arguments that come from shader tables.
	{
		RayTracing::PipelineLayout pipelineLayout;
		pipelineLayout.SetConstants(0, SizeOfInUint32(XMFLOAT4[2]), 0);
		m_rayGenPipelineLayout = pipelineLayout.GetPipelineLayout(m_device, m_pipelineLayoutCache,
			PipelineLayoutFlag::LOCAL_PIPELINE_LAYOUT, NumUAVs);
	}

#if 1
	// Local Root Signature for Hit shader
	// This is a root signature that enables a shader to have unique arguments that come from shader tables.
	{
		RayTracing::PipelineLayout pipelineLayout;
		pipelineLayout.SetRange(0, DescriptorType::SRV, 2, 1);
		//pipelineLayout.SetRange(1, DescriptorType::SRV, static_cast<uint32_t>(size(m_textures)), 0, 1);
		m_hitGroupPipelineLayout = pipelineLayout.GetPipelineLayout(m_device, m_pipelineLayoutCache,
			PipelineLayoutFlag::LOCAL_PIPELINE_LAYOUT, NumUAVs);
	}
#endif
}

void DXRTest::CreateRaytracingPipeline()
{
	m_pipelineCache.SetDevice(m_device);
	N_RETURN(m_shaderPool.CreateShader(Shader::Stage::CS, 0, L"RayTraceShader.cso"), ThrowIfFailed(E_FAIL));

	RayTracing::State state;
	state.SetShaderLibrary(m_shaderPool.GetShader(Shader::Stage::CS, 0));
	state.SetHitGroup(0, HitGroupName, ClosestHitShaderName);
	state.SetShaderConfig(sizeof(XMFLOAT4), sizeof(XMFLOAT2));
	state.SetLocalPipelineLayout(0, m_rayGenPipelineLayout,
		1, reinterpret_cast<const void**>(&RaygenShaderName));
	//state.SetLocalPipelineLayout(1, m_hitGroupPipelineLayout,
		//1, reinterpret_cast<const void**>(&HitGroupName));
	state.SetGlobalPipelineLayout(m_globalPipelineLayout);
	state.SetMaxRecursionDepth(1);
	m_pipeline = state.GetPipeline(m_pipelineCache);
}

void DXRTest::CreateDescriptorTables(vector<Descriptor>& descriptors)
{
	{
		descriptors.push_back(m_bottomLevelAS.GetResult().GetUAV());
		descriptors.push_back(m_topLevelAS.GetResult().GetUAV());
		// 3 UAVs, so NumUAVs = 3.
		Util::DescriptorTable descriptorTable;
		descriptorTable.SetDescriptors(0, static_cast<uint32_t>(descriptors.size()), descriptors.data());
		m_uavTable = descriptorTable.GetCbvSrvUavTable(m_descriptorTableCache);
	}

	{
		Descriptor descriptors[] = { m_indexBuffer.GetSRV(), m_vertexBuffer.GetSRV() };
		Util::DescriptorTable descriptorTable;
		descriptorTable.SetDescriptors(0, static_cast<uint32_t>(size(descriptors)), descriptors);
		m_srvIATable = descriptorTable.GetCbvSrvUavTable(m_descriptorTableCache);
	}

	{
		vector<Descriptor> descriptors;
		descriptors.reserve(_countof(m_textures));
		for (const auto& texture : m_textures)
			descriptors.push_back(texture.GetSRV());
		Util::DescriptorTable descriptorTable;
		descriptorTable.SetDescriptors(0, static_cast<uint32_t>(descriptors.size()), descriptors.data());
		m_srvTextureTable = descriptorTable.GetCbvSrvUavTable(m_descriptorTableCache);
	}
}

void DXRTest::BuildAccelerationStructures(uint32_t numDescs, Geometry* geometries,
	Resource& scratchResource, Resource& instanceDescs)
{
	// Set geometries
	BottomLevelAS::SetGeometries(geometries, numDescs, Format::R32G32B32_FLOAT,
		&m_vertexBuffer.GetVBV(), &m_indexBuffer.GetIBV());

	// Descriptors
	vector<Descriptor> descriptors = { m_raytracingOutput.GetUAV() };
	const uint32_t bottomLevelASIndex = static_cast<uint32_t>(descriptors.size());
	const uint32_t topLevelASIndex = bottomLevelASIndex + 1;

	// Prebuild
	m_bottomLevelAS.PreBuild(m_device, numDescs, geometries, bottomLevelASIndex, NumUAVs);
	m_topLevelAS.PreBuild(m_device, numDescs, topLevelASIndex, NumUAVs);

	// Create scratch buffer
	const auto scratchSize = (max)(m_bottomLevelAS.GetScratchDataMaxSize(), m_topLevelAS.GetScratchDataMaxSize());
	AccelerationStructure::AllocateUAVBuffer(m_device, scratchResource, scratchSize);

	// Get descriptor pool and create descriptor tables
	CreateDescriptorTables(descriptors);

	// Set instance
	const auto numInstances = 1u;
	XMFLOAT4X4 matrix;
	XMStoreFloat4x4(&matrix, XMMatrixIdentity());
	float* const transforms[] = { *matrix.m };
	TopLevelAS::SetInstances(m_device, instanceDescs, 1, &m_bottomLevelAS, transforms);

	// Build bottom level AS
	m_bottomLevelAS.Build(m_commandList, scratchResource,
		m_descriptorTableCache.GetDescriptorPool(CBV_SRV_UAV_POOL), NumUAVs);

	// Build top level AS
	m_topLevelAS.Build(m_commandList, scratchResource, instanceDescs,
		m_descriptorTableCache.GetDescriptorPool(CBV_SRV_UAV_POOL), NumUAVs);
}

void DXRTest::BuildShaderTables()
{
	// Get shader identifiers.
	const auto shaderIDSize = ShaderRecord::GetShaderIDSize(m_device);

	// Ray gen shader table
	XMFLOAT4 rayGenArgs[] = { m_cbData_Viewport, m_cbData_Offset };
	m_rayGenShaderTable.Create(m_device, 1, shaderIDSize + sizeof(rayGenArgs), L"RayGenShaderTable");
	m_rayGenShaderTable.AddShaderRecord(ShaderRecord(m_device, m_pipeline,
		RaygenShaderName, rayGenArgs, sizeof(rayGenArgs)));

	// Miss shader table
	m_missShaderTable.Create(m_device, 1, shaderIDSize, L"MissShaderTable");
	m_missShaderTable.AddShaderRecord(ShaderRecord(m_device, m_pipeline, MissShaderName));

	// Hit group shader table
	//DescriptorView descriptorArgs[] = { (*m_srvIATable), };//(*m_srvTextureTable) };
	//m_hitGroupShaderTable.Create(m_device, 1, shaderIDSize + sizeof(descriptorArgs), L"HitGroupShaderTable");
	//m_hitGroupShaderTable.AddShaderRecord(ShaderRecord(m_device, m_raytracingPipeline, HitGroupName,
		//descriptorArgs, sizeof(descriptorArgs)));
	m_hitGroupShaderTable.Create(m_device, 1, shaderIDSize, L"HitGroupShaderTable");
	m_hitGroupShaderTable.AddShaderRecord(ShaderRecord(m_device, m_pipeline, HitGroupName));
}

void DXRTest::RayTrace()
{
	m_commandList.SetComputePipelineLayout(m_globalPipelineLayout);

	// Bind the heaps, acceleration structure and dispatch rays.
	const DescriptorPool descriptorPools[] =
	{
		m_descriptorTableCache.GetDescriptorPool(CBV_SRV_UAV_POOL),
		m_descriptorTableCache.GetDescriptorPool(SAMPLER_POOL)
	};
	m_commandList.SetDescriptorPools(2, descriptorPools);

	m_commandList.SetComputeDescriptorTable(0, m_uavTable);
	m_commandList.SetTopLevelAccelerationStructure(1, m_topLevelAS);
	m_commandList.SetComputeDescriptorTable(2, m_samplerTable);
	m_commandList.SetComputeDescriptorTable(3, m_srvIATable);
	m_commandList.SetComputeDescriptorTable(4, m_srvTextureTable);

	m_commandList.DispatchRays(m_pipeline, m_width, m_height, 1,
		m_hitGroupShaderTable, m_missShaderTable, m_rayGenShaderTable);
}

// Copy the raytracing output to the backbuffer.
void DXRTest::CopyRaytracingOutputToBackbuffer()
{
	ResourceBarrier barriers[2];
	auto numBarriers = m_raytracingOutput.SetBarrier(barriers, ResourceState::COPY_SOURCE);
	numBarriers = m_renderTargets[m_frameIndex].SetBarrier(barriers, ResourceState::COPY_DEST, numBarriers);
	m_commandList.Barrier(numBarriers, barriers);
	m_commandList.CopyResource(m_renderTargets[m_frameIndex].GetResource(), m_raytracingOutput.GetResource());
	numBarriers = m_renderTargets[m_frameIndex].SetBarrier(barriers, ResourceState::PRESENT);
	m_commandList.Barrier(numBarriers, barriers);
}
